from class_hunter import main_class_hunter
from functional_hunter import main_functional_hunter


def test_matrix_from_test_task():
    input_matrix = [
        [55, 14, 25, 52, 21],
        [44, 31, 11, 53, 43],
        [24, 13, 45, 12, 34],
        [42, 22, 43, 32, 41],
        [51, 23, 33, 54, 15]
    ]

    output_from_functional_hunter = main_functional_hunter(input_matrix)
    output_from_class_hunter = main_class_hunter(input_matrix)

    assert output_from_functional_hunter == [11, 55, 15, 21, 44, 32, 13, 25, 43]
    assert output_from_class_hunter == [11, 55, 15, 21, 44, 32, 13, 25, 43]


def test_matrix_with_treasure_at_start():
    input_matrix = [
        [11, 21],
        [22, 12]
    ]

    output_from_functional_hunter = main_functional_hunter(input_matrix)
    output_from_class_hunter = main_class_hunter(input_matrix)

    assert output_from_functional_hunter == [11]
    assert output_from_class_hunter == [11]


def test_matrix_without_treasure():
    input_matrix = [
        [31, 21, 11],
        [32, 21, 22],
        [33, 23, 13]
    ]

    output_from_functional_hunter = main_functional_hunter(input_matrix)
    output_from_class_hunter = main_class_hunter(input_matrix)

    assert output_from_functional_hunter == []
    assert output_from_class_hunter == []
