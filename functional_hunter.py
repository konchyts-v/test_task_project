from collections import deque


def treasure_searching(q, input_matrix, path):
    if not q:
        return

    coord_value = q.popleft()

    if coord_value in path:
        path.clear()
        return

    value = input_matrix[coord_value // 10 - 1][coord_value % 10 - 1]
    path.append(coord_value)

    if not coord_value == value:
        q.append(value)

    treasure_searching(q, input_matrix, path)


def main_functional_hunter(input_matrix):
    q = deque()
    path = list()
    
    q.append(11)

    treasure_searching(q, input_matrix, path)

    return path


if __name__ == '__main__':
    input_matrix = [
        [55, 14, 25, 52, 21],
        [44, 31, 11, 53, 43],
        [24, 13, 45, 12, 34],
        [42, 22, 43, 32, 41],
        [51, 23, 33, 54, 15]
    ]

    print(main_functional_hunter(input_matrix))
