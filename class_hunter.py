from collections import deque


class TreasureHunter:

    def __init__(self, input_matrix):
        self._input_matrix = input_matrix
        self._q = deque()
        self._path = list()

    def searching(self):
        self._q.append(11)

        while self._q:
            coord_value = self._q.popleft()

            if coord_value in self._path:
                self._path.clear()
                return

            value = self._input_matrix[coord_value // 10 - 1][coord_value % 10 - 1]
            self._path.append(coord_value)

            if not coord_value == value:
                self._q.append(value)

    def get_result(self):
        return self._path


def main_class_hunter(input_matrix):
    treasure_hunter = TreasureHunter(input_matrix)
    treasure_hunter.searching()

    return treasure_hunter.get_result()


if __name__ == '__main__':
    input_matrix = [
        [55, 14, 25, 52, 21],
        [44, 31, 11, 53, 43],
        [24, 13, 45, 12, 34],
        [42, 22, 43, 32, 41],
        [51, 23, 33, 54, 15]
    ]

    print(main_class_hunter(input_matrix))
